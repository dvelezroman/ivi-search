import React from 'react';
import './App.css';
import FirstScreen from './containers/FirstScreen';

function App() {
	return (
		<div className='App'>
			<FirstScreen />
		</div>
	);
}

export default App;
